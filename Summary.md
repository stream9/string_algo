
# Brief summary of changes

## Case Conversion
Obviously, **to_upper** and **to_lower**'s inputs have to be a Sequence, but that is not the case for **to_upper_copy** and **to_lower_copy**. Still, current interface decide output type from input type therefore input has to be a Sequence. So I add a overload that take extra template parameter as output type. [Code](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/to_upper.hpp#L20), [Usage](https://gitlab.com/stream9/string_algo/blob/master/test/case_conv.cpp#L64)

## Trimming
Trimming should work on string_view for both input and output, but current code doesn't handle it. 

- **trim_left**, **trim_right**, **trim** uses **erase** to trim input string, but for **string_view** this has to be done with **remove_prefix**, **remove_suffix** function. So I change input from Sequence to Range and branch on whether input has appropriate function or not.[Code](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/trim.hpp#L17), [Usage](https://gitlab.com/stream9/string_algo/blob/master/test/trim.cpp#L18)

- **trim_copy** series: I thought these could hanle **string_view** out of the box, but it can't. Problem is that **string_view** doesn't have constructor which take a pair of iterators like Sequence types do. So I made a little adjustment. [Code](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/trim.hpp#L61), [Usage](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/trim.hpp#L61)

## Find algorithm
These functions return **iterator_range**. It is fine especially unlike **string_view**, **iterator_range** preserve input's iterator which is important I think. But I also think it is nice if I could specify return type manually so it can return more string-ish type. So I add an overload. [Code](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/find.hpp#L33), [Usage](https://gitlab.com/stream9/string_algo/blob/master/test/find.cpp). I also made the change that if Range has passed instead Finder argument, transform it to **first_finder** so I think I can reduce the number of overloads, but you can ignore that.

## Erase / Replace
Same as case conversion, **_copy** series of these functions should be abled to take **string_view** as input but they can't because thay require Sequence as input. So I made a change that allow me to specify output type separately. [Code](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/find_replace.hpp#L50), [Usage](https://gitlab.com/stream9/string_algo/blob/master/test/find_replace.cpp#L124) Here I rename **find_format** to **find_replace** (I though that would be more intuitive), but you can ignore that.

## Find iterator
This work fine on **string_view** but as same as **find** functions, its output value type is fixed to **iterator_range**. Therefore I modify it so I can manually specify output value type if I want to. I also add **begin** and **end** function overload so it works fine with C++11  range for loop. [Code](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/find_iterator.hpp#L16), [Usage](https://gitlab.com/stream9/string_algo/blob/master/test/find_iterator.cpp#L122)

## Split iterator
Same as Find iterator [Code](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/split_iterator.hpp#L19), [Usage](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/split_iterator.hpp#L19)

## ''split'' and ''find_all''
These are basically wrapper for **find_iterator**, **split_iterator**, but it doesn't work as is, because again **string_view**'s lack of constructor that take a pair of iterator. So I made a few tweak. [Code](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/split.hpp#L17), [Usage](https://gitlab.com/stream9/string_algo/blob/master/test/split.cpp#L104)

## Join
**join**'s return type also determine by input type, so it doesn't work on input like **std::vector<std::string_view>**. I change that so I can specify output type separately to input type.[Code](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/join.hpp#L22), [Usage](https://gitlab.com/stream9/string_algo/blob/master/test/join.cpp#L32)

## Regex
Current code assume regex is a **boost::regex**, but C++11 have **std::regex**. So I think it would be nice if I can choose which regex to use. There are a lot of overload function related to regex in the library, but it all boils down to **regex_finder** and **regex_formatter**. So I wrote **std::regex** version of **regex_finder** and **regex_formatter**. [Code](https://gitlab.com/stream9/string_algo/blob/master/include/algorithm/string/std_regex.hpp#L12), [Usage](https://gitlab.com/stream9/string_algo/blob/master/test/std_regex.cpp#L52)
Both **std::regex_match** and **boost::regex_match** doesn't take **string_view** as input (it take iterator instead) but because of changes I made here, all function relate to regex can now take string_view. It's a nice side effect.
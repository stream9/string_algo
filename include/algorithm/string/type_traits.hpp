#ifndef ALGORITHM_TYPE_TRAITS_HPP
#define ALGORITHM_TYPE_TRAITS_HPP

#include <algorithm/string/remove_prefix.hpp>
#include <algorithm/string/remove_suffix.hpp>

#include <cstddef>
#include <type_traits>

#include <boost/range/iterator.hpp>
#include <boost/iterator/iterator_traits.hpp>

namespace algorithm::string {

/*
 * has_remove_prefix
 */
template<typename T>
struct has_remove_prefix
{
    template<typename U>
    static auto check(U u) -> decltype(remove_prefix(u, 0), std::true_type());

    static std::false_type check(...);

    static bool const value = decltype(check(std::declval<T>()))::value;
};

template<typename T>
static constexpr bool has_remove_prefix_v = has_remove_prefix<T>::value;

/*
 * has_remove_suffix
 */
template<typename T>
struct has_remove_suffix
{
    template<typename U>
    static auto check(U u) -> decltype(remove_suffix(u, 0), std::true_type());

    static std::false_type check(...);

    static bool const value = decltype(check(std::declval<T>()))::value;
};

template<typename T>
static constexpr bool has_remove_suffix_v = has_remove_suffix<T>::value;

/*
 * has_erase
 */
template<typename T>
struct has_erase
{
    template<typename U>
    static auto check(U u)
            -> decltype(u.erase(u.begin(), u.end()), std::true_type());

    static std::false_type check(...);

    static bool const value = decltype(check(std::declval<T>()))::value;
};

template<typename T>
static constexpr bool has_erase_v = has_erase<T>::value;

/*
 * copy_dest
 */
template<typename SequenceT, typename RangeT>
using copy_dest_t =
    std::conditional_t<std::is_same_v<SequenceT, void>,
                       RangeT, SequenceT>;

/*
 * range_iterator
 */
template<typename RangeT>
using range_iterator_t = typename ::boost::range_iterator<RangeT>::type;

template<typename RangeT>
using range_const_iterator_t =
            typename ::boost::range_const_iterator<RangeT>::type;

/*
 * range_value
 */
template<typename RangeT>
using range_value_t = typename ::boost::range_value<RangeT>::type;

/*
 * iterator_value
 */
template<typename It>
using iterator_value_t = typename ::boost::iterator_value<It>::type;

/*
 * is_predicate
 */
template<typename T, typename CharT>
static bool constexpr is_predicate_v =
                    std::is_invocable_r_v<bool, T, CharT>;

/*
 * is_finder
 */
template<typename FinderT, typename IteratorT>
static bool constexpr is_finder_v  =
    std::is_invocable_v<
        FinderT,
        IteratorT, IteratorT
    >;

/*
 * is_formatter
 */
template<typename FormatterT, typename FinderResultT>
static bool constexpr is_formatter_v  =
    std::is_invocable_v<
        FormatterT,
        FinderResultT
    >;

} // namespace algorithm::string

#endif // ALGORITHM_TYPE_TRAITS_HPP

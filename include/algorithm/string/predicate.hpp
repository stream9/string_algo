#ifndef ALGORITHM_STRING_PREDICATE_HPP
#define ALGORITHM_STRING_PREDICATE_HPP

#include <boost/algorithm/string/predicate.hpp>

namespace algorithm::string {

namespace ba = ::boost::algorithm;

using ba::starts_with,
      ba::istarts_with,
      ba::ends_with,
      ba::iends_with,
      ba::contains,
      ba::icontains,
      ba::equals,
      ba::iequals,
      ba::lexicographical_compare,
      ba::ilexicographical_compare,
      ba::all;

} // namespace algorithm::string

#endif // ALGORITHM_STRING_PREDICATE_HPP

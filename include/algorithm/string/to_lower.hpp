#ifndef ALGORITHM_STRING_TO_LOWER_HPP
#define ALGORITHM_STRING_TO_LOWER_HPP

#include <algorithm/string/type_traits.hpp>

#include <locale>

#include <boost/algorithm/string/case_conv.hpp>

namespace algorithm::string {

namespace { namespace ba = boost::algorithm; }

using ba::to_lower;
using ba::to_lower_copy;

/*
 * to_lower_copy
 */
template<typename SequenceT, typename RangeT>
SequenceT
to_lower_copy(RangeT const& input, std::locale loc = std::locale())
{
    return ba::detail::transform_range_copy<SequenceT>(
        input,
        ba::detail::to_lowerF<range_value_t<SequenceT>>(loc)
    );
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_TO_LOWER_HPP

#ifndef ALGORITHM_STRING_JOIN_HPP
#define ALGORITHM_STRING_JOIN_HPP

#include <algorithm/string/type_traits.hpp>

#include <boost/algorithm/string/join.hpp>

namespace algorithm::string {

namespace ba = boost::algorithm;

namespace {

template<typename ResultSequenceT, typename RangeSequenceT>
using join_result_t = std::conditional_t<
    std::is_same_v<ResultSequenceT, void>,
    range_value_t<RangeSequenceT>,
    ResultSequenceT >;

} // namespace

template<typename ResultSequenceT = void,
         typename RangeSequenceT, typename RangeT, typename PredicateT>
auto
join_if(RangeSequenceT const& input, RangeT const& separator,
        PredicateT pred)
{
    join_result_t<ResultSequenceT, RangeSequenceT> result;

    if (input.empty()) return result;

    using ::std::begin, ::std::end;
    auto it = begin(input);
    auto const e = end(input);

    if (pred(*it)) {
        ba::detail::insert(result, end(result), *it);
    }
    ++it;

    for (; it != e; ++it) {
        if (!pred(*it)) continue;

        // Add separator
        ba::detail::insert(result, end(result), ::boost::as_literal(separator));
        // Add element
        ba::detail::insert(result, end(result), *it);
    }

    return result;
}

template<typename ResultSequenceT = void,
         typename RangeSequenceT, typename RangeT>
auto
join(RangeSequenceT const& input, RangeT const& delim)
{
    return join_if<ResultSequenceT>(
                        input, delim, [](auto) { return true; });
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_JOIN_HPP

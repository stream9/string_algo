#ifndef ALGORITHM_STRING_REGEX_HPP
#define ALGORITHM_STRING_REGEX_HPP

#include <boost/algorithm/string/regex_find_format.hpp>

namespace algorithm::string {

namespace ba = ::boost::algorithm;

using ba::regex_finder;
using ba::regex_formatter;

} // namespace algorithm::string

#endif // ALGORITHM_STRING_REGEX_HPP

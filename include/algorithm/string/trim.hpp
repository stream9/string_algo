#ifndef ALGORITHM_STRING_TRIM_HPP
#define ALGORITHM_STRING_TRIM_HPP

#include <algorithm/string/type_traits.hpp>
#include <algorithm/string/detail/make_range.hpp>
#include <algorithm/string/classification.hpp>

#include <iterator>

#include <boost/algorithm/string/trim.hpp>

namespace algorithm::string {

namespace ba = ::boost::algorithm;
namespace cur = ::algorithm::string;

/*
 * left trim
 */
template<typename RangeT, typename PredicateT>
void
trim_left_if(RangeT& input, PredicateT is_space)
{
    static_assert(has_erase_v<RangeT> || has_remove_prefix_v<RangeT>);

    if constexpr (has_erase_v<RangeT>) {
        ba::trim_left_if(input, is_space);
    }
    else { // has_remove_prefix_v<RangeT>
        using std::begin, std::end;

        auto const it = ba::detail::trim_begin(
            begin(input),
            end(input),
            is_space);
        assert(it >= begin(input));

        size_t const n = it - begin(input);

        remove_prefix(input, n);
    }
}

template<typename RangeT>
void
trim_left(RangeT& input, std::locale const& loc = std::locale())
{
    static_assert(has_erase_v<RangeT> || has_remove_prefix_v<RangeT>);

    cur::trim_left_if(input, is_space(loc));
}

template<typename OutputIteratorT, typename RangeT, typename PredicateT>
OutputIteratorT
trim_left_copy_if(OutputIteratorT output, RangeT const& input,
                  PredicateT is_space)
{
    return ba::trim_left_copy_if(output, input, is_space);
}

template<typename RangeT, typename PredicateT>
RangeT
trim_left_copy_if(RangeT const& input, PredicateT is_space)
{
    using std::begin, std::end;

    return detail::make_range<RangeT>(
        ba::detail::trim_begin(
            begin(input),
            end(input),
            is_space
        ),
        end(input));
}

template<typename RangeT>
RangeT
trim_left_copy(RangeT const& input, std::locale const& loc = std::locale())
{
    return cur::trim_left_copy_if(input, is_space(loc));
}

/*
 * trim right
 */
template<typename RangeT, typename PredicateT>
void
trim_right_if(RangeT& input, PredicateT is_space)
{
    static_assert(has_erase_v<RangeT> || has_remove_suffix_v<RangeT>);

    if constexpr (has_erase_v<RangeT>) {
        ba::trim_right_if(input, is_space);
    }
    else { // has_remove_suffix_v<RangeT>
        using std::begin, std::end;

        auto const it = ba::detail::trim_end(
            begin(input),
            end(input),
            is_space);
        assert(input.end() >= it);

        size_t const n = input.end() - it;

        remove_suffix(input, n);
    }
}

template<typename RangeT>
void
trim_right(RangeT& input, std::locale const& loc = std::locale())
{
    static_assert(has_erase_v<RangeT> || has_remove_suffix_v<RangeT>);

    cur::trim_right_if(input, is_space(loc));
}

template<typename OutputIteratorT, typename RangeT, typename PredicateT>
OutputIteratorT
trim_right_copy_if(OutputIteratorT output, RangeT const& input,
                  PredicateT is_space)
{
    return ba::trim_right_copy_if(output, input, is_space);
}

template<typename RangeT, typename PredicateT>
RangeT
trim_right_copy_if(RangeT const& input, PredicateT is_space)
{
    using std::begin, std::end;

    auto const from = begin(input);
    auto const to = ba::detail::trim_end(
        from,
        end(input),
        is_space);
    assert(input.end() >= to);

    return detail::make_range<RangeT>(from, to);
}

template<typename RangeT>
RangeT
trim_right_copy(RangeT const& input, std::locale const& loc = std::locale())
{
    return cur::trim_right_copy_if(input, is_space(loc));
}

/*
 * trim both
 */
template<typename RangeT, typename PredicateT>
void
trim_if(RangeT& input, PredicateT is_space)
{
    static_assert(has_erase_v<RangeT> ||
        (has_remove_prefix_v<RangeT> && has_remove_suffix_v<RangeT>) );

    cur::trim_left_if(input, is_space);
    cur::trim_right_if(input, is_space);
}

template<typename RangeT>
void
trim(RangeT& input, std::locale const& loc = std::locale())
{
    static_assert(has_erase_v<RangeT> ||
        (has_remove_prefix_v<RangeT> && has_remove_suffix_v<RangeT>) );

    cur::trim_if(input, is_space(loc));
}

template<typename OutputIteratorT, typename RangeT, typename PredicateT>
OutputIteratorT
trim_copy_if(OutputIteratorT output, RangeT const& input,
                  PredicateT is_space)
{
    return ba::trim_copy_if(output, input, is_space);
}

template<typename RangeT, typename PredicateT>
RangeT
trim_copy_if(RangeT const& input, PredicateT is_space)
{
    using std::begin, std::end;

    auto const to = ba::detail::trim_end(
        begin(input),
        end(input),
        is_space);
    auto const from = ba::detail::trim_begin(
        begin(input),
        to,
        is_space);

    assert(from <= to);

    return detail::make_range<RangeT>(from, to);
}

template<typename RangeT>
RangeT
trim_copy(RangeT const& input, std::locale const& loc = std::locale())
{
    return cur::trim_copy_if(input, is_space(loc));
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_TRIM_HPP

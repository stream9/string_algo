#ifndef ALGORITHM_STRING_FINDER_HPP
#define ALGORITHM_STRING_FINDER_HPP

#include <algorithm/string/detail/finder.hpp>
#include <algorithm/string/type_traits.hpp>

#include <boost/algorithm/string/finder.hpp>

namespace algorithm::string {

namespace cur = algorithm::string;
namespace ba = ::boost::algorithm;

using ba::first_finder;
using ba::last_finder;
using ba::nth_finder;
using ba::head_finder;
using ba::tail_finder;
using ba::token_finder;
using ba::range_finder;

template<typename IteratorT, typename FinderT>
constexpr auto
make_finder(FinderT finder)
{
    if constexpr (is_finder_v<FinderT, IteratorT>) {
        return finder;
    }
    else {
        return cur::first_finder(finder);
    }
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_FINDER_HPP

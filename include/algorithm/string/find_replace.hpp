#ifndef ALGORITHM_STRING_FIND_REPLACE_HPP
#define ALGORITHM_STRING_FIND_REPLACE_HPP

#include <algorithm/string/finder.hpp>
#include <algorithm/string/formatter.hpp>
#include <algorithm/string/type_traits.hpp>

#include <boost/algorithm/string/find_format.hpp>

namespace algorithm::string {

namespace cur = algorithm::string;
namespace ba = ::boost::algorithm;

/*
 * find_replace
 */
template<typename SequenceT, typename FinderT, typename FormatterT>
void find_replace(SequenceT& input,
                  FinderT finder, FormatterT formatter)
{
    auto finder_ = make_finder<range_iterator_t<SequenceT>>(finder);
    using finder_result_t = decltype(finder_(input.begin(), input.end()));

    auto formatter_ = make_formatter<finder_result_t>(formatter);

    ba::find_format(input, finder_, formatter_);
}

/*
 * find_replace_copy
 */
template<typename OutputIteratorT, typename RangeT,
         typename FinderT, typename FormatterT>
OutputIteratorT
find_replace_copy(OutputIteratorT output,
                  RangeT const& input,
                  FinderT finder,
                  FormatterT formatter)
{
    auto finder_ = make_finder<range_iterator_t<RangeT>>(finder);

    using finder_result_t = decltype(finder_(input.begin(), input.end()));

    auto formatter_ = make_formatter<finder_result_t>(formatter);

    return ba::find_format_copy(output, input, finder_, formatter_);
}

template<typename SequenceT = void, typename RangeT,
         typename FinderT, typename FormatterT>
copy_dest_t<SequenceT, RangeT>
find_replace_copy(RangeT const& input,
                  FinderT finder,
                  FormatterT formatter)
{
    auto finder_ = make_finder<range_iterator_t<RangeT>>(finder);

    using finder_result_t = decltype(finder_(input.begin(), input.end()));

    auto formatter_ = make_formatter<finder_result_t>(formatter);

    if constexpr (std::is_same_v<SequenceT, void>) {
        return ba::find_format_copy(input, finder_, formatter_);
    }
    else {
        //TODO properly
        SequenceT result { ::boost::begin(input), ::boost::end(input) };

        ba::find_format(result, finder_, formatter_);

        return result;
    }
}

/*
 * find_replace_all
 */
template<typename SequenceT, typename FinderT, typename FormatterT>
void find_replace_all(SequenceT& input,
                      FinderT finder, FormatterT formatter)
{
    auto finder_ = make_finder<range_iterator_t<SequenceT>>(finder);

    using finder_result_t = decltype(finder_(input.begin(), input.end()));

    auto formatter_ = make_formatter<finder_result_t>(formatter);

    ba::find_format_all(input, finder_, formatter_);
}

/*
 * find_replace_all_copy
 */
template<typename OutputIteratorT, typename RangeT,
         typename FinderT, typename FormatterT>
OutputIteratorT
find_replace_all_copy(OutputIteratorT output,
                      RangeT const& input,
                      FinderT finder,
                      FormatterT formatter)
{
    auto finder_ = make_finder<range_iterator_t<RangeT>>(finder);

    using finder_result_t = decltype(finder_(input.begin(), input.end()));

    auto formatter_ = make_formatter<finder_result_t>(formatter);

    return ba::find_format_all_copy(output, input, finder_, formatter_);
}

template<typename SequenceT = void, typename RangeT,
         typename FinderT, typename FormatterT>
copy_dest_t<SequenceT, RangeT>
find_replace_all_copy(RangeT const& input,
                      FinderT finder,
                      FormatterT formatter)
{
    auto finder_ = make_finder<range_iterator_t<RangeT>>(finder);

    using finder_result_t = decltype(finder_(input.begin(), input.end()));

    auto formatter_ = make_formatter<finder_result_t>(formatter);

    if constexpr (std::is_same_v<SequenceT, void>) {
        return ba::find_format_all_copy(input, finder_, formatter_);
    }
    else {
        //TODO properly
        SequenceT result { ::boost::begin(input), ::boost::end(input) };

        ba::find_format_all(result, finder_, formatter_);

        return result;
    }
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_FIND_REPLACE_HPP

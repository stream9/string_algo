#ifndef ALGORITHM_STRING_FORMATTER_HPP
#define ALGORITHM_STRING_FORMATTER_HPP

#include <algorithm/string/type_traits.hpp>

#include <boost/algorithm/string/formatter.hpp>

namespace algorithm::string {

namespace cur = algorithm::string;
namespace ba = ::boost::algorithm;

using ba::const_formatter,
      ba::identity_formatter,
      ba::empty_formatter;

template<typename FinderResultT, typename FormatterT>
constexpr auto
make_formatter(FormatterT formatter)
{
    if constexpr (cur::is_formatter_v<FormatterT, FinderResultT>) {
        return formatter;
    }
    else {
        return cur::const_formatter(formatter);
    }
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_FORMATTER_HPP

#ifndef ALGORITHM_STRING_FIND_ITERATOR_HPP
#define ALGORITHM_STRING_FIND_ITERATOR_HPP

#include <algorithm/string/finder.hpp>
#include <algorithm/string/type_traits.hpp>
#include <algorithm/string/utility.hpp>

#include <string_view>

#include <boost/algorithm/string/find_iterator.hpp>

namespace algorithm::string {

namespace ba = ::boost::algorithm;

/*
 * find_iterator
 */
template<typename IteratorT,
         typename ValueRangeT = ::boost::iterator_range<IteratorT> >
class find_iterator : public ba::find_iterator<IteratorT>
{
public:
    using parent_t = ba::find_iterator<IteratorT>;
    using parent_t::operator=;

    find_iterator() = default;

    template<typename RangeT, typename FinderT>
    find_iterator(RangeT const& string, FinderT const& finder)
        : parent_t { string, make_finder<IteratorT>(finder) }
    {}

    template<typename FinderT>
    find_iterator(IteratorT const begin, IteratorT const end,
                  FinderT const& finder)
        : parent_t { begin, end, make_finder<IteratorT>(finder) }
    {}

    auto operator*() const
    {
        return cur::copy_range<ValueRangeT>(parent_t::operator*());
    }
};

/*
 * make_find_iterator
 */
template<typename ValueRangeT = void,
         typename RangeT, typename FinderT>
auto
make_find_iterator(RangeT const& string, FinderT const& finder)
{
    using iterator_t = range_const_iterator_t<RangeT>;

    if constexpr (std::is_same_v<ValueRangeT, void>) {
        return find_iterator<iterator_t> {
            string, finder
        };
    }
    else {
        return find_iterator<iterator_t, ValueRangeT> {
                string, finder
        };
    }
}

/*
 * user-defined deduction guide
 */
template<typename RangeT, typename FinderT>
find_iterator(RangeT const&, FinderT const&)
    -> find_iterator<range_const_iterator_t<RangeT>>;

/*
 * begin
 */
template<typename IteratorT, typename ValueRangeT>
find_iterator<IteratorT, ValueRangeT>
begin(find_iterator<IteratorT, ValueRangeT> const it)
{
    return it;
}

/*
 * end
 */
template<typename IteratorT, typename ValueRangeT>
find_iterator<IteratorT, ValueRangeT>
end(find_iterator<IteratorT, ValueRangeT>)
{
    return {};
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_FIND_ITERATOR_HPP

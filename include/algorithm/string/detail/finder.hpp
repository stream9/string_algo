#ifndef ALGORITHM_STRING_DETAIL_FINDER_HPP
#define ALGORITHM_STRING_DETAIL_FINDER_HPP

#include <algorithm/string/type_traits.hpp>
#include <algorithm/string/utility.hpp>

#include <string_view>

#include <boost/algorithm/string/detail/finder.hpp>

namespace algorithm::string::detail {

namespace ba = ::boost::algorithm;

template<typename SearchIteratorT,typename PredicateT>
struct first_finderF : ba::detail::first_finderF<SearchIteratorT, PredicateT>
{
    using parent_t = ba::detail::first_finderF<SearchIteratorT, PredicateT>;
    using parent_t::parent_t;
    using parent_t::operator=;

    // Operation
    template<typename ForwardIteratorT>
    std::basic_string_view<iterator_value_t<ForwardIteratorT>>
    operator()(ForwardIteratorT const begin, ForwardIteratorT const end) const
    {
        return to_string_view(parent_t::operator()(begin, end));
    }
};

} // namespace algorithm::string::detail

#endif // ALGORITHM_STRING_DETAIL_FINDER_HPP

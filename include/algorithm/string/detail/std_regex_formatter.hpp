#ifndef ALGORITHM_STRING_DETAIL_STD_REGEX_FORMATTER_HPP
#define ALGORITHM_STRING_DETAIL_STD_REGEX_FORMATTER_HPP

#include <regex>

#include <algorithm/string/detail/std_regex_finder.hpp>

namespace algorithm::string::detail {

/*
 * std_regex_formatF
 */
template<typename StringT>
struct std_regex_formatF
{
private:
    typedef StringT result_type;
    typedef typename StringT::value_type char_type;
    typedef std::regex_constants::match_flag_type match_flag_type;

public:
    // Construction
    std_regex_formatF( const StringT& Fmt,
        match_flag_type Flags=std::regex_constants::format_default ) :
        m_Fmt(Fmt), m_Flags( Flags ) {}

    template<typename InputIteratorT>
    result_type operator()(
        const std_regex_search_result<InputIteratorT>& Replace ) const
    {
        if ( Replace.empty() )
        {
            return result_type();
        }
        else
        {
            return Replace.match_results().format( m_Fmt, m_Flags );
        }
    }
private:
    const StringT& m_Fmt;
    match_flag_type m_Flags;
};

} // namespace algorithm::string::detail

#endif // ALGORITHM_STRING_DETAIL_STD_REGEX_FORMATTER_HPP

#ifndef ALGORITHM_STRING_DETAIL_MAKE_RANGE_HPP
#define ALGORITHM_STRING_DETAIL_MAKE_RANGE_HPP

#include <cassert>
#include <cstddef>

namespace algorithm::string::detail {

template<typename RangeT, typename It>
auto
make_range(It const begin, It const end)
    -> decltype(RangeT(begin, end))
{
    assert(begin <= end);

    return { begin, end };
}

template<typename RangeT, typename It>
auto
make_range(It const begin, It const end)
    -> decltype(RangeT(begin, static_cast<size_t>(end - begin)))
{
    assert(begin <= end);

    return { begin, static_cast<size_t>(end - begin) };
}

} // namespace algorithm::string::detail

#endif // ALGORITHM_STRING_DETAIL_MAKE_RANGE_HPP

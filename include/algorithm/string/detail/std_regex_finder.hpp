#ifndef ALGORITHM_STRING_DETAIL_STD_REGEX_FINDER_HPP
#define ALGORITHM_STRING_DETAIL_STD_REGEX_FINDER_HPP

#include <regex>

#include <boost/range/iterator_range.hpp>

namespace algorithm::string::detail {

/*
 * std_regex_search_result
 */
template<typename IteratorT>
struct std_regex_search_result : public boost::iterator_range<IteratorT>
{
    typedef std_regex_search_result<IteratorT> type;
    typedef boost::iterator_range<IteratorT> base_type;
    typedef typename  base_type::value_type value_type;
    typedef typename  base_type::difference_type difference_type;
    typedef typename  base_type::const_iterator const_iterator;
    typedef typename  base_type::iterator iterator;
    typedef ::std::match_results<iterator> match_results_type;

    // Construction

    // Construction from the match result
    std_regex_search_result( const match_results_type& MatchResults ) :
        base_type( MatchResults[0].first, MatchResults[0].second ),
        m_MatchResults( MatchResults ) {}

    // Construction of empty match. End iterator has to be specified
    std_regex_search_result( IteratorT End ) :
        base_type( End, End ) {}

    std_regex_search_result( const std_regex_search_result& Other ) :
        base_type( Other.begin(), Other.end() ),
        m_MatchResults( Other.m_MatchResults ) {}

    // Assignment
    std_regex_search_result& operator=( const std_regex_search_result& Other )
    {
        base_type::operator=( Other );
        m_MatchResults=Other.m_MatchResults;
        return *this;
    }

    // Match result retrieval
    const match_results_type& match_results() const
    {
        return m_MatchResults;
    }

private:
    // Saved match result
    match_results_type m_MatchResults;
};


/*
 * find_std_regexF
 */
template<typename RegExT>
struct find_std_regexF
{
    typedef RegExT regex_type;
    typedef const RegExT& regex_reference_type;

    // Construction
    find_std_regexF(regex_reference_type Rx,
        std::regex_constants::match_flag_type MatchFlags = std::regex_constants::match_default) :
            m_Rx(Rx), m_MatchFlags(MatchFlags) {}

    // Operation
    template< typename ForwardIteratorT >
    std_regex_search_result<ForwardIteratorT>
    operator()(
        ForwardIteratorT Begin,
        ForwardIteratorT End ) const
    {
        typedef ForwardIteratorT input_iterator_type;
        typedef std_regex_search_result<ForwardIteratorT> result_type;

        // instantiate match result
        ::std::match_results<input_iterator_type> result;
        // search for a match
        if ( ::std::regex_search( Begin, End, result, m_Rx, m_MatchFlags ) )
        {
            // construct a result
            return result_type( result );
        }
        else
        {
            // empty result
            return result_type( End );
        }
    }

private:
    regex_reference_type m_Rx; // Regexp
    std::regex_constants::match_flag_type m_MatchFlags;     // match flags
};

} // namespace algorithm::string::detail

#endif // ALGORITHM_STRING_DETAIL_STD_REGEX_FINDER_HPP

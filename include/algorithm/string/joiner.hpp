#ifndef ALGORITHM_STRING_JOINER_HPP
#define ALGORITHM_STRING_JOINER_HPP

#include <string>
#include <string_view>

namespace algorithm::string {

class joiner
{
public:
    joiner(std::string delim)
        : m_delim { std::move(delim) } {}

    joiner& operator=(std::string_view const s)
    {
        if (!m_str.empty()) {
            m_str.append(m_delim);
        }

        m_str.append(s);

        return *this;
    }

    std::string const& str() const& { return m_str; }
    std::string const& str() & { return m_str; }
    std::string&& str() && { return std::move(m_str); }

private:
    std::string m_str;
    std::string m_delim;
};

inline joiner&
operator<<(joiner& j, std::string_view const& s)
{
    j = s;
    return j;
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_JOINER_HPP

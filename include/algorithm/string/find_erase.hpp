#ifndef ALGORITHM_STRING_FIND_ERASE_HPP
#define ALGORITHM_STRING_FIND_ERASE_HPP

#include <algorithm/string/find_replace.hpp>
#include <algorithm/string/finder.hpp>
#include <algorithm/string/formatter.hpp>
#include <algorithm/string/type_traits.hpp>

namespace algorithm::string {

namespace cur = algorithm::string;

/*
 * find_erase
 */
template<typename SequenceT, typename FinderT>
void
find_erase(SequenceT& input, FinderT finder)
{
    auto finder_ = make_finder<range_iterator_t<SequenceT>>(finder);

    cur::find_replace(input, finder_, cur::empty_formatter(input));
}

/*
 * find_erase_copy
 */
template<typename OutputIteratorT, typename RangeT, typename FinderT>
OutputIteratorT
find_erase_copy(OutputIteratorT output,
                RangeT const& input,
                FinderT finder)
{
    auto finder_ = make_finder<range_iterator_t<RangeT>>(finder);

    return cur::find_replace_copy(
                output, input, finder_, cur::empty_formatter(input));
}

template<typename SequenceT = void, typename RangeT, typename FinderT>
copy_dest_t<SequenceT, RangeT>
find_erase_copy(RangeT const& input,
                FinderT finder)
{
    auto finder_ = make_finder<range_iterator_t<RangeT>>(finder);

    return cur::find_replace_copy<SequenceT>(
                        input, finder_, cur::empty_formatter(input));
}

/*
 * find_erase_all
 */
template<typename SequenceT, typename FinderT>
void find_erase_all(SequenceT& input, FinderT finder)
{
    auto finder_ = make_finder<range_iterator_t<SequenceT>>(finder);

    cur::find_replace_all(input, finder_, cur::empty_formatter(input));
}

/*
 * find_erase_all_copy
 */
template<typename OutputIteratorT, typename RangeT, typename FinderT>
OutputIteratorT
find_erase_all_copy(OutputIteratorT output,
                    RangeT const& input,
                    FinderT finder)
{
    auto finder_ = make_finder<range_iterator_t<RangeT>>(finder);

    return cur::find_replace_all_copy(
                    output, input, finder_, cur::empty_formatter(input));
}

template<typename SequenceT = void, typename RangeT, typename FinderT>
copy_dest_t<SequenceT, RangeT>
find_erase_all_copy(RangeT const& input, FinderT finder)
{
    auto finder_ = make_finder<range_iterator_t<RangeT>>(finder);

    return cur::find_replace_all_copy<SequenceT>(
                    input, finder_, cur::empty_formatter(input));
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_FIND_ERASE_HPP

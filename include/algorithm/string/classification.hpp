#ifndef ALGORITHM_STRING_CLASSIFICATION_HPP
#define ALGORITHM_STRING_CLASSIFICATION_HPP

#include <boost/algorithm/string/classification.hpp>

namespace algorithm::string {

namespace ba = ::boost::algorithm;

using ba::is_classified,
      ba::is_space,
      ba::is_alnum,
      ba::is_alpha,
      ba::is_cntrl,
      ba::is_digit,
      ba::is_graph,
      ba::is_lower,
      ba::is_print,
      ba::is_punct,
      ba::is_upper,
      ba::is_xdigit,
      ba::is_any_of,
      ba::is_from_range;

} // namespace algorithm::string

#endif // ALGORITHM_STRING_CLASSIFICATION_HPP

#ifndef ALGORITHM_STRING_REMOVE_PREFIX_HPP
#define ALGORITHM_STRING_REMOVE_PREFIX_HPP

#include <cstddef>

namespace algorithm::string {

template<typename RangeT>
auto remove_prefix(RangeT& r, size_t const n)
        -> decltype(r.remove_prefix(n), void())
{
    r.remove_prefix(n);
}

template<typename RangeT>
auto remove_prefix(RangeT& r, size_t const n)
        -> decltype(r.drop_front(n), void())
{
    r.drop_front(n);
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_REMOVE_PREFIX_HPP

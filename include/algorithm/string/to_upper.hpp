#ifndef ALGORITHM_STRING_TO_UPPER_HPP
#define ALGORITHM_STRING_TO_UPPER_HPP

#include <algorithm/string/type_traits.hpp>

#include <locale>

#include <boost/algorithm/string/case_conv.hpp>

namespace algorithm::string {

namespace { namespace ba = boost::algorithm; }

using ba::to_upper;
using ba::to_upper_copy;

/*
 * to_upper_copy
 */
template<typename SequenceT, typename RangeT>
SequenceT
to_upper_copy(RangeT const& input, std::locale loc = std::locale())
{
    return ba::detail::transform_range_copy<SequenceT>(
        input,
        ba::detail::to_upperF<range_value_t<SequenceT>>(loc)
    );
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_TO_UPPER_HPP

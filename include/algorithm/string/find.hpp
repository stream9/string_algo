#ifndef ALGORITHM_STRING_FIND_HPP
#define ALGORITHM_STRING_FIND_HPP

#include <algorithm/string/constants.hpp>
#include <algorithm/string/finder.hpp>
#include <algorithm/string/find_iterator.hpp>
#include <algorithm/string/utility.hpp>
#include <algorithm/string/type_traits.hpp>

#include <iterator>

#include <boost/algorithm/string/find.hpp>
#include <boost/iterator/transform_iterator.hpp>

namespace algorithm::string {

namespace cur = ::algorithm::string;
namespace ba = ::boost::algorithm;

/*
 * find
 */
template<typename RangeT, typename FinderT>
boost::iterator_range<range_iterator_t<RangeT>>
find(RangeT& input, FinderT const& finder)
{
    auto finder_ =
        cur::make_finder<range_const_iterator_t<RangeT>>(finder);

    return ba::find(input, finder_);
}

template<typename ResultRangeT, typename RangeT, typename FinderT>
ResultRangeT
find(RangeT& input, FinderT const& finder)
{
    auto finder_ =
        cur::make_finder<range_const_iterator_t<RangeT>>(finder);

    return cur::copy_range<ResultRangeT>(ba::find(input, finder_));
}

/*
 * find_first
 */
using ba::find_first;
using ba::ifind_first;

/*
 * find_last
 */
using ba::find_last;
using ba::ifind_last;

/*
 * find_nth
 */
using ba::find_nth;
using ba::ifind_nth;

/*
 * find_head
 */
using ba::find_head;

/*
 * find_tail
 */
using ba::find_tail;

/*
 * find_token
 */
using ba::find_token;

/*
 * find_all
 */
template<typename RangeSequenceT,
         typename RangeT, typename FinderT>
RangeSequenceT&
find_all(RangeSequenceT& output,
         RangeT const& input,
         FinderT finder)
{
    auto finder_ =
        cur::make_finder<range_const_iterator_t<RangeT>>(finder);

    auto copy_range = [](auto const& r) {
            using sequence_t = range_value_t<RangeSequenceT>;
            return cur::copy_range<sequence_t>(r);
        };

    auto it = ::boost::make_transform_iterator(
        cur::make_find_iterator(input, finder_),
        copy_range);

    auto const end = ::boost::make_transform_iterator(
        std::decay_t<decltype(it.base())>(),
        copy_range);

    RangeSequenceT tmp { it, end };

    output.swap(tmp);

    return output;
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_FIND_HPP

#ifndef ALGORITHM_STRING_SPLIT_HPP
#define ALGORITHM_STRING_SPLIT_HPP

#include <algorithm/string/split_iterator.hpp>

#include <iterator>

#include <boost/iterator/transform_iterator.hpp>

namespace algorithm::string {

namespace cur = algorithm::string;

namespace detail {

template<typename RangeSequenceT, typename IteratorT>
RangeSequenceT&
split_impl(RangeSequenceT& result, IteratorT const& it)
{
    auto copy_range = [](auto const& r) {
        using sequence_t = range_value_t<RangeSequenceT>;
        return cur::copy_range<sequence_t>(r);
    };

    auto const begin =
        ::boost::make_transform_iterator(it, copy_range);
    auto const end =
        ::boost::make_transform_iterator(IteratorT(), copy_range);

    RangeSequenceT tmp { begin, end };

    result.swap(tmp);

    return result;
}

} // namespace detail

template<typename RangeSequenceT, typename RangeT>
RangeSequenceT&
split(RangeSequenceT& result, RangeT const& input)
{
    return detail::split_impl(result, cur::split_iterator(input));
}

template<typename RangeSequenceT, typename RangeT, typename FinderT,
    typename = std::enable_if_t<
        !is_predicate_v<FinderT, range_value_t<RangeT>> >>
RangeSequenceT&
split(RangeSequenceT& result,
      RangeT const& input, FinderT const& finder)
{
    return detail::split_impl(result, cur::split_iterator(input, finder));
}

template<typename RangeSequenceT, typename RangeT, typename PredicateT,
    typename = std::enable_if_t<
        is_predicate_v<PredicateT, range_value_t<RangeT>> >>
RangeSequenceT&
split(RangeSequenceT& result,
      RangeT const& input,
      PredicateT const& pred,
      token_compress_mode_type const mode = token_compress_off)
{
    return detail::split_impl(result, cur::split_iterator(input, pred, mode));
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_SPLIT_HPP

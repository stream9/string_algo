#ifndef ALGORITHM_STRING_CONSTANTS_HPP
#define ALGORITHM_STRING_CONSTANTS_HPP

#include <boost/algorithm/string/constants.hpp>

namespace algorithm::string {

namespace ba = ::boost::algorithm;

using ba::token_compress_mode_type;
using ba::token_compress_on;
using ba::token_compress_off;

} // namespace algorith::string

#endif // ALGORITHM_STRING_CONSTANTS_HPP

#ifndef ALGORITHM_STRING_STRING_RANGE_HPP
#define ALGORITHM_STRING_STRING_RANGE_HPP

#include <algorithm/string/utility.hpp>

#include <boost/range/iterator_range.hpp>
#include <boost/range/as_literal.hpp>

namespace algorithm::string {

template<typename IteratorT>
static constexpr bool is_string_iterator_v =
    std::is_same_v<iterator_value_t<IteratorT>, char>;

template<typename IteratorT>
static constexpr bool is_string_range_v =
    std::is_same_v<range_value_t<IteratorT>, char>;

#if 0
template<typename IteratorT,
    typename = std::enable_if_t<
        is_string_iterator_v<IteratorT>> >
class string_range : private boost::iterator_range<IteratorT>
{
    using parent_t = boost::iterator_range<IteratorT>;

public:
    using parent_t::parent_t;
    using parent_t::operator=;

    using parent_t::begin;
    using parent_t::end;

    //using parent_t::operator bool;
    using parent_t::equal;
    using parent_t::front;
    using parent_t::back;
    using parent_t::advance_begin;
    using parent_t::advance_end;
    using parent_t::empty;

    using parent_t::operator[];
    using parent_t::size;
};
#endif

template<typename IteratorT,
    typename = std::enable_if_t<
        is_string_iterator_v<IteratorT>> >
class string_range
{
    using range_t = boost::iterator_range<IteratorT>;
public:
    string_range() = default;

    template<typename RangeT>
    string_range(RangeT& r)
        : range_t { r }
    {}

private:
    range_t m_range;
};
/*
 * user-defined deduction guide
 */
template<typename RangeT>
string_range(RangeT& r)
    -> string_range<range_iterator_t<RangeT>>;

template<typename IteratorT>
string_range(IteratorT begin, IteratorT end)
    -> string_range<IteratorT>;

/*
 * operators
 */
template<typename IteratorT>
bool operator==(string_range<IteratorT> const& lhs,
//inline bool operator==(string_range<std::string::iterator> const& lhs,
                char const* const rhs)
{
    return lhs == boost::as_literal(rhs);
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_STRING_RANGE_HPP

#ifndef ALGORITH_STRING_UTILITY_HPP
#define ALGORITH_STRING_UTILITY_HPP

#include <algorithm/string/type_traits.hpp>

#include <cassert>
#include <string_view>

#include <boost/range/begin.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/size.hpp>

namespace algorithm::string {

template<typename RangeT>
std::basic_string_view<range_value_t<RangeT>>
to_string_view(RangeT const& r)
{
    return { &*::boost::begin(r), ::boost::size(r) };
}

template<typename It>
std::basic_string_view<iterator_value_t<It>>
to_string_view(It const begin, It const end)
{
    assert(begin <= end);

    return { begin, static_cast<size_t>(end - begin) };
}

template<typename SequenceT, typename RangeT>
SequenceT
copy_range(RangeT const& r)
{
    if constexpr (std::is_same_v<SequenceT, std::string_view>) {
        return { &*::boost::begin(r), ::boost::size(r) };
    }
    else {
        return ::boost::copy_range<SequenceT>(r);
    }
}

} // namespace algorithm::string

#endif // ALGORITH_STRING_UTILITY_HPP

#ifndef ALGORITHM_STRING_REMOVE_SUFFIX_HPP
#define ALGORITHM_STRING_REMOVE_SUFFIX_HPP

#include <cstddef>

namespace algorithm::string {

template<typename RangeT>
auto remove_suffix(RangeT& r, size_t const n)
        -> decltype(r.remove_suffix(n), void())
{
    r.remove_suffix(n);
}

template<typename RangeT>
auto remove_suffix(RangeT& r, size_t const n)
        -> decltype(r.drop_front(n), void())
{
    r.drop_front(n);
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_REMOVE_SUFFIX_HPP

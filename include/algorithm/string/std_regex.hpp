#ifndef ALGORITHM_STRING_REGEX_HPP
#define ALGORITHM_STRING_REGEX_HPP

#include <string>
#include <regex>

#include <algorithm/string/detail/std_regex_finder.hpp>
#include <algorithm/string/detail/std_regex_formatter.hpp>

namespace algorithm::string {

template<typename CharT, typename RegexTraitsT>
detail::find_std_regexF<std::basic_regex<CharT, RegexTraitsT>>
std_regex_finder(std::basic_regex<CharT, RegexTraitsT> const& rx,
    std::regex_constants::match_flag_type flags = std::regex_constants::match_default)
{
    return detail::find_std_regexF<
        std::basic_regex<CharT, RegexTraitsT>>(rx, flags);
}

template<typename CharT, typename TraitsT, typename AllocT>
detail::std_regex_formatF<std::basic_string<CharT, TraitsT, AllocT>>
std_regex_formatter(std::basic_string<CharT, TraitsT, AllocT> const& format,
                std::regex_constants::match_flag_type flags = std::regex_constants::format_default)
{
    return detail::std_regex_formatF<
        std::basic_string<CharT, TraitsT, AllocT>>(format, flags);
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_REGEX_HPP

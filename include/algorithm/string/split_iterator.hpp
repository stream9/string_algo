#ifndef ALGORITHM_STRING_SPLIT_ITERATOR_HPP
#define ALGORITHM_STRING_SPLIT_ITERATOR_HPP

#include <algorithm/string/classification.hpp>
#include <algorithm/string/constants.hpp>
#include <algorithm/string/finder.hpp>
#include <algorithm/string/type_traits.hpp>
#include <algorithm/string/utility.hpp>

#include <string_view>

#include <boost/algorithm/string/find_iterator.hpp>

namespace algorithm::string {

namespace ba = ::boost::algorithm;
namespace cur = algorithm::string;

/*
 * split_iterator
 */
template<typename IteratorT,
         typename ValueRangeT = ::boost::iterator_range<IteratorT> >
class split_iterator : public ba::split_iterator<IteratorT>
{
public:
    using parent_t = ba::split_iterator<IteratorT>;
    using parent_t::operator=;

    split_iterator() = default;

    /*
     * construct with default finder
     */
    template<typename RangeT>
    split_iterator(RangeT const& string)
        : parent_t { string, make_token_finder() }
    {}

    /*
     * construct with token finder
     */
    template<typename RangeT, typename PredicateT,
        typename = std::enable_if_t<
            is_predicate_v<PredicateT, range_value_t<RangeT>> >>
    split_iterator(RangeT const& string,
                   PredicateT pred,
                   token_compress_mode_type const mode = token_compress_off)
        : parent_t { string, make_token_finder(pred, mode) }
    {}

    template<typename RangeT, typename FinderT,
        typename = std::enable_if_t<
            !is_predicate_v<FinderT, range_value_t<RangeT>> >>
    split_iterator(RangeT const& string, FinderT const& finder)
        : parent_t { string, make_finder<IteratorT>(finder) }
    {}

    template<typename FinderT>
    split_iterator(IteratorT const begin, IteratorT const end,
                   FinderT const& finder)
        : parent_t { begin, end, make_finder<IteratorT>(finder) }
    {}

    auto operator*() const
    {
        return cur::copy_range<ValueRangeT>(parent_t::operator*());
    }

private:
    static auto make_token_finder()
    {
        return make_token_finder(is_space(), token_compress_off);
    }

    template<typename PredicateT>
    static auto make_token_finder(
                    PredicateT pred,
                    token_compress_mode_type const mode)
    {
        return cur::token_finder(pred, mode);
    }
};

/*
 * user-defined deduction guide
 */
template<typename RangeT>
split_iterator(RangeT const&)
    -> split_iterator<range_const_iterator_t<RangeT>>;

template<typename RangeT, typename PredicateT,
    typename = std::enable_if_t<
        is_predicate_v<PredicateT, range_value_t<RangeT>> >>
split_iterator(RangeT const&, PredicateT,
               token_compress_mode_type const mode = token_compress_off)
    -> split_iterator<range_const_iterator_t<RangeT>>;

template<typename RangeT, typename FinderT,
    typename = std::enable_if_t<
        !is_predicate_v<FinderT, range_value_t<RangeT>> >>
split_iterator(RangeT const&, FinderT const&)
    -> split_iterator<range_const_iterator_t<RangeT>>;

/*
 * make_split_iterator
 */
template<typename ValueRangeT = void, typename RangeT>
auto
make_split_iterator(RangeT const& string)
{
    if constexpr (std::is_same_v<ValueRangeT, void>) {
        return split_iterator<range_const_iterator_t<RangeT>> {
                string
        };
    }
    else {
        return split_iterator<
            range_const_iterator_t<RangeT>, ValueRangeT> {
                string
        };
    }
}

template<typename ValueRangeT = void, typename RangeT, typename FinderT,
    typename = std::enable_if_t<
        !is_predicate_v<FinderT, range_value_t<RangeT>> >>
auto
make_split_iterator(RangeT const& string, FinderT const& finder)
{
    if constexpr (std::is_same_v<ValueRangeT, void>) {
        return split_iterator<range_const_iterator_t<RangeT>> {
            string, finder
        };
    }
    else {
        return split_iterator<
            range_const_iterator_t<RangeT>, ValueRangeT> {
                string, finder
        };
    }
}

template<typename ValueRangeT = void, typename RangeT, typename PredicateT,
    typename = std::enable_if_t<
        is_predicate_v<PredicateT, range_value_t<RangeT>> >>
auto
make_split_iterator(RangeT const& string, PredicateT pred,
            token_compress_mode_type const mode = token_compress_off)
{
    if constexpr (std::is_same_v<ValueRangeT, void>) {
        return split_iterator<range_const_iterator_t<RangeT>> {
            string, pred, mode
        };
    }
    else {
        return split_iterator<
            range_const_iterator_t<RangeT>, ValueRangeT> {
                string, pred, mode
        };
    }
}

/*
 * begin
 */
template<typename IteratorT, typename ValueRangeT>
split_iterator<IteratorT, ValueRangeT>
begin(split_iterator<IteratorT, ValueRangeT> const it)
{
    return it;
}

/*
 * end
 */
template<typename IteratorT, typename ValueRangeT>
split_iterator<IteratorT, ValueRangeT>
end(split_iterator<IteratorT, ValueRangeT>)
{
    return {};
}

} // namespace algorithm::string

#endif // ALGORITHM_STRING_SPLIT_ITERATOR_HPP

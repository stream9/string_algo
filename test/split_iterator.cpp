#include <algorithm/string/split_iterator.hpp>

#include "utility.hpp"

#include <algorithm/string/finder.hpp>

#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(split_iterator_)

BOOST_AUTO_TEST_SUITE(constructor_)

    template<typename IteratorT>
    void check(IteratorT it)
    {
        auto n = 0;
        for (auto s: it) {
            BOOST_CHECK_EQUAL(s, "foo"_r);
            ++n;
        }
        BOOST_TEST(n == 4);
    }

    BOOST_AUTO_TEST_CASE(range_and_default_finder)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator<std::string::const_iterator> it {
            s1
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_default_finder_return_string_view)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator<
            std::string::const_iterator, std::string_view> it {
                s1
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_default_finder_with_deduction_guide)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator it { s1 };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_token_finder1)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator<std::string::const_iterator> it {
            s1, as::is_space(), as::token_compress_off
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_token_finder1_with_deduction_guide)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator it {
            s1, as::is_space(), as::token_compress_off
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_token_finder2)
    {
        std::string const s1 = "foo  foo    foo     foo";

        as::split_iterator<std::string::const_iterator> it {
            s1, as::is_space(), as::token_compress_on
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_token_finder2_with_deduction_guide)
    {
        std::string const s1 = "foo  foo    foo     foo";

        as::split_iterator it {
            s1, as::is_space(), as::token_compress_on
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_token_finder3)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator<std::string::const_iterator> it {
            s1, as::is_space()
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_token_finder3_with_deduction_guide)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator it {
            s1, as::is_space()
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_finder)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator<std::string::const_iterator> it {
            s1, as::first_finder(" ")
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_finder_with_deduction_guide)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator it {
            s1, as::first_finder(" ")
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_range)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator<std::string::const_iterator> it {
            s1, " "
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_range_with_deduction_guide)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator it {
            s1, " "
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(iterator_and_finder)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator it {
            s1.begin(), s1.end(), as::first_finder(" ")
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(iterator_and_range)
    {
        std::string const s1 = "foo foo foo foo";

        as::split_iterator it {
            s1.begin(), s1.end(), " "
        };

        check(it);
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(make_split_iterator_)

    template<typename IteratorT>
    void check(IteratorT it)
    {
        auto n = 0;
        for (auto s: it) {
            BOOST_CHECK_EQUAL(s, "foo"_r);
            ++n;
        }
        BOOST_TEST(n == 4);
    }

    BOOST_AUTO_TEST_CASE(string_with_default_finder)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator(s1);

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_with_default_finder_return_string_view)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator<std::string_view>(s1);

        BOOST_TEST(type_is<std::string_view>(*it));

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_with_finder)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator(s1, as::first_finder(" "));

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_with_finder_return_string_view)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator<std::string_view>(
                                            s1, as::first_finder(" "));

        BOOST_TEST(type_is<std::string_view>(*it));

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_with_range)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator(s1, " ");

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_with_range_return_string_view)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator<std::string_view>(s1, " ");

        BOOST_TEST(type_is<std::string_view>(*it));

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_with_predicate)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator(s1, as::is_space());

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_with_predicate_return_string_view)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator<std::string_view>(s1, as::is_space());

        BOOST_TEST(type_is<std::string_view>(*it));

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_view_with_default_finder)
    {
        std::string_view const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator(s1);

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_view_with_finder)
    {
        std::string_view const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator(s1, as::first_finder(" "));

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range)
    {
        std::string_view const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator(s1, " ");

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_view_with_predicate)
    {
        std::string_view const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator(s1, as::is_space());

        check(it);
    }

BOOST_AUTO_TEST_SUITE_END() // make_split_iterator_

BOOST_AUTO_TEST_SUITE(ranged_for_support_)

    BOOST_AUTO_TEST_CASE(begin_)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator(s1, as::first_finder(" "));

        using std::begin;
        BOOST_TEST((begin(it) == it));
    }

    BOOST_AUTO_TEST_CASE(end_)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_split_iterator(s1, as::first_finder(" "));

        using std::end;
        BOOST_TEST(end(it).eof());
    }

BOOST_AUTO_TEST_SUITE_END() // ranged_for_support_

BOOST_AUTO_TEST_SUITE_END() // split_iterator_

} // namespace testing


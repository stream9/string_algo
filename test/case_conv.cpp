#include <algorithm/string/to_upper.hpp>
#include <algorithm/string/to_lower.hpp>

#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>
#include <boost/range/iterator_range.hpp>

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(case_conv)

BOOST_AUTO_TEST_SUITE(to_upper)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = "foo";

        as::to_upper(s1);

        BOOST_TEST(s1 == "FOO");
    }

BOOST_AUTO_TEST_SUITE_END() // to_upper

BOOST_AUTO_TEST_SUITE(to_upper_copy_iterator)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo";
        std::string s2;

        as::to_upper_copy(std::back_inserter(s2), s1);

        BOOST_TEST(s2 == "FOO");
    }

    BOOST_AUTO_TEST_CASE(string_view_to_string)
    {
        std::string_view const s1 = "foo";
        std::string s2;

        as::to_upper_copy(std::back_inserter(s2), s1);

        BOOST_TEST(s2 == "FOO");
    }

BOOST_AUTO_TEST_SUITE_END() // to_upper_copy_iterator

BOOST_AUTO_TEST_SUITE(to_upper_copy_)

    BOOST_AUTO_TEST_CASE(string_to_string)
    {
        std::string const s1 = "foo";

        auto const& s2 = as::to_upper_copy(s1);

        BOOST_TEST(s2 == "FOO");
    }

    BOOST_AUTO_TEST_CASE(string_view_to_string)
    {
        std::string_view const s1 = "foo";

        auto const& s2 = as::to_upper_copy<std::string>(s1);

        BOOST_TEST(s2 == "FOO");
    }

    BOOST_AUTO_TEST_CASE(iterator_range_to_string)
    {
        auto const s1 = boost::as_literal("foo");

        auto const& s2 = as::to_upper_copy<std::string>(s1);

        BOOST_TEST(s2 == "FOO");
    }

BOOST_AUTO_TEST_SUITE_END() // to_upper_copy_

BOOST_AUTO_TEST_SUITE(to_lower_copy_)

    BOOST_AUTO_TEST_CASE(string_to_string)
    {
        std::string const s1 = "FOO";

        auto const& s2 = as::to_lower_copy(s1);

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view_to_string)
    {
        std::string_view const s1 = "FOO";

        auto const& s2 = as::to_lower_copy<std::string>(s1);

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(iterator_range_to_string)
    {
        auto const s1 = boost::as_literal("FOO");

        auto const& s2 = as::to_lower_copy<std::string>(s1);

        BOOST_TEST(s2 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // to_lower_copy_

BOOST_AUTO_TEST_SUITE_END() // case_conv_

} // namespace testing


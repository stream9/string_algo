#include <algorithm/string/string_range.hpp>

#include <boost/test/unit_test.hpp>

#include <boost/range/iterator_range.hpp>

#include <iostream>

namespace boost {

template<typename RangeT>
static constexpr bool is_string_range_v =
       std::is_same_v<RangeT, boost::iterator_range<char const*>>
    || std::is_same_v<RangeT, boost::iterator_range<char*>>
    || std::is_same_v<RangeT, boost::iterator_range<std::string::iterator>>
    || std::is_same_v<RangeT, boost::iterator_range<std::string::const_iterator>>
    ;

template<typename RangeT>
std::enable_if_t<is_string_range_v<RangeT>, bool>
operator==(RangeT const& lhs, char const* rhs)
{
    std::cout << "overload1\n";
    return lhs == boost::as_literal(rhs);
}

template<typename RangeT>
std::enable_if_t<is_string_range_v<RangeT>, bool>
operator==(char const* lhs, RangeT const& rhs)
{
    std::cout << "overload2\n";
    return boost::as_literal(lhs) == rhs;
}

} // namespace boost

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(string_range_)

#if 0
    void test()
    {
        //type_of<decltype(r1)> is1;

        std::string const s2 = "bar";
        as::string_range r2 { s2 };

        //type_of<decltype(r2)> is2;

        auto rr1 = boost::make_iterator_range(s1);
        //as::string_range r3 { rr1 };
        as::string_range r3 { rr1.begin(), rr1.end() };
    }
#endif

    BOOST_AUTO_TEST_CASE(build)
    {
        //std::string s1 = "foo";
        //as::string_range r1 { s1 };
    }

BOOST_AUTO_TEST_SUITE_END() // string_range_

BOOST_AUTO_TEST_CASE(iterator_range_)
{
#if 0
    std::string s1 = "foo";
    auto r1 = boost::make_iterator_range(s1);

    BOOST_TEST(r1 == "foo");
#endif
}

} // namespace testing


#include <algorithm/string/join.hpp>

#include "utility.hpp"

#include <algorithm/string/finder.hpp>

#include <string>
#include <string_view>
#include <vector>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(join_)

    BOOST_AUTO_TEST_CASE(strings_to_string)
    {
        std::vector<std::string> const v {
            "foo", "bar", "xyzzy"
        };

        auto const s = as::join(v, ", ");

        static_assert(type_is<std::string>(s));

        BOOST_TEST(s == "foo, bar, xyzzy");
    }

    BOOST_AUTO_TEST_CASE(string_views_to_string)
    {
        std::vector<std::string_view> const v {
            "foo", "bar", "xyzzy"
        };

        auto const s = as::join<std::string>(v, ", ");

        static_assert(type_is<std::string>(s));

        BOOST_TEST(s == "foo, bar, xyzzy");
    }

BOOST_AUTO_TEST_SUITE_END() // join_

BOOST_AUTO_TEST_SUITE(join_if_)

    BOOST_AUTO_TEST_CASE(strings_to_string)
    {
        std::vector<std::string> const v {
            "foo", "bar", "wee", "xyzzy"
        };

        auto const s = as::join_if(v, ", ",
            [](auto& s) {
                return s != "wee";
            });

        static_assert(type_is<std::string>(s));

        BOOST_TEST(s == "foo, bar, xyzzy");
    }

    BOOST_AUTO_TEST_CASE(string_views_to_string)
    {
        std::vector<std::string_view> const v {
            "foo", "bar", "wee", "xyzzy"
        };

        auto const s = as::join_if<std::string>(v, ", ",
            [](auto& s) {
                return s != "wee";
            });

        static_assert(type_is<std::string>(s));

        BOOST_TEST(s == "foo, bar, xyzzy");
    }


BOOST_AUTO_TEST_SUITE_END() // join_if_

} // namespace testing


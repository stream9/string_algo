#ifndef ALGORITHM_STRING_TEST_UTILITY_HPP
#define ALGORITHM_STRING_TEST_UTILITY_HPP

#include <type_traits>

#include <boost/range/iterator_range.hpp>

#include <boost/test/unit_test.hpp>

BOOST_TEST_DONT_PRINT_LOG_VALUE(std::string::iterator)
BOOST_TEST_DONT_PRINT_LOG_VALUE(std::string::const_iterator)

namespace testing {

template<typename> struct type_of;

template<typename U, typename T>
constexpr bool
type_is(T const& v)
{
    return std::is_same_v<std::decay_t<decltype(v)>, U>;
}

template<typename T, typename U>
constexpr bool
is_same_type(T const&, U const&)
{
    return std::is_same_v<
        std::decay_t<T>,
        std::decay_t<U>
    >;
}

inline auto
operator""_r(char const* const c_str, std::size_t const n)
{
    return boost::make_iterator_range(c_str, c_str + n);
}

} // namespace testing

namespace boost {

inline bool
operator==(std::string::const_iterator const lhs, char const* const rhs)
{
    return &*lhs == rhs;
}

inline bool
operator==(char const* const lhs, std::string::const_iterator const rhs)
{
    return lhs == &*rhs;
}

} // namespace boost

#endif // ALGORITHM_STRING_TEST_UTILITY_HPP

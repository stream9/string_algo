#include <algorithm/string/split.hpp>

#include "utility.hpp"

#include <algorithm/string/finder.hpp>

#include <algorithm>
#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(split_)

    template<typename ResultT>
    void check(ResultT const& o, ResultT const&)
    {
        BOOST_TEST(o.size() == 4);
        std::all_of(o.begin(), o.end(), [](auto& s) { return s == "foo"; });
    }

BOOST_AUTO_TEST_SUITE(with_default_finder)

    BOOST_AUTO_TEST_CASE(string_to_strings)
    {
        std::string const s1 = "foo foo foo foo";
        std::vector<std::string> o;

        auto& r = as::split(o, s1);

        check(o, r);
    }

    BOOST_AUTO_TEST_CASE(string_to_string_views)
    {
        std::string const s1 = "foo foo foo foo";
        std::vector<std::string_view> o;

        auto& r = as::split(o, s1);

        check(o, r);
    }

    BOOST_AUTO_TEST_CASE(string_to_iterator_ranges)
    {
        std::string const s1 = "foo foo foo foo";
        std::vector<boost::iterator_range<std::string::const_iterator>> o;

        auto& r = as::split(o, s1);

        check(o, r);
    }

    BOOST_AUTO_TEST_CASE(string_view_to_strings)
    {
        std::string_view const s1 = "foo foo foo foo";
        std::vector<std::string> o;

        auto& r = as::split(o, s1);

        check(o, r);
    }

    BOOST_AUTO_TEST_CASE(string_view_to_string_views)
    {
        std::string_view const s1 = "foo foo foo foo";
        std::vector<std::string_view> o;

        auto& r = as::split(o, s1);

        check(o, r);
    }

BOOST_AUTO_TEST_SUITE_END() // with_default_finder

BOOST_AUTO_TEST_SUITE(with_range)

    BOOST_AUTO_TEST_CASE(string_to_strings)
    {
        std::string const s1 = "foo  foo  foo  foo";
        std::vector<std::string> o;

        auto& r = as::split(o, s1, "  ");

        check(o, r);
    }

    BOOST_AUTO_TEST_CASE(string_to_string_views)
    {
        std::string const s1 = "foo  foo  foo  foo";
        std::vector<std::string_view> o;

        auto& r = as::split(o, s1, "  ");

        check(o, r);
    }

BOOST_AUTO_TEST_SUITE_END() // with_range

BOOST_AUTO_TEST_SUITE(with_finder)

    BOOST_AUTO_TEST_CASE(string_to_strings)
    {
        std::string const s1 = "foo  foo  foo  foo";
        std::vector<std::string> o;

        auto& r = as::split(o, s1, as::first_finder("  "));

        check(o, r);
    }

    BOOST_AUTO_TEST_CASE(string_to_string_views)
    {
        std::string const s1 = "foo  foo  foo  foo";
        std::vector<std::string_view> o;

        auto& r = as::split(o, s1, as::first_finder("  "));

        check(o, r);
    }

BOOST_AUTO_TEST_SUITE_END() // with_finder

BOOST_AUTO_TEST_SUITE(with_predicate)

    BOOST_AUTO_TEST_CASE(string_to_strings)
    {
        std::string const s1 = "foo foo foo foo";
        std::vector<std::string> o;

        auto& r = as::split(o, s1, as::is_space());

        check(o, r);
    }

    BOOST_AUTO_TEST_CASE(string_to_string_views)
    {
        std::string const s1 = "foo  foo  foo  foo";
        std::vector<std::string_view> o;

        auto& r = as::split(o, s1, as::is_space(), as::token_compress_on);

        check(o, r);
    }

BOOST_AUTO_TEST_SUITE_END() // with_predicate

BOOST_AUTO_TEST_SUITE_END() // split_

} // namespace testing

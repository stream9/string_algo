#include <algorithm/string/find_replace.hpp>

#include "utility.hpp"

#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

#include <algorithm/string/finder.hpp>
#include <algorithm/string/formatter.hpp>

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(find_replace_)

BOOST_AUTO_TEST_SUITE(find_replace_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = "foo";

        as::find_replace(s1,
            as::first_finder("f"),
            as::const_formatter("b"));

        BOOST_TEST(s1 == "boo");
    }

    BOOST_AUTO_TEST_CASE(string_with_range)
    {
        std::string s1 = "foo";

        as::find_replace(s1,
            "f",
            "b"
        );

        BOOST_TEST(s1 == "boo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_

BOOST_AUTO_TEST_SUITE(find_replace_copy_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo";

        auto const s2 = as::find_replace_copy(s1,
            as::first_finder("f"),
            as::const_formatter("b"));

        BOOST_TEST(s2 == "boo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_copy_

BOOST_AUTO_TEST_SUITE(find_replace_copy_iterator)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo";
        std::string s2;

        as::find_replace_copy(
            std::back_inserter(s2),
            s1,
            as::first_finder("f"),
            as::const_formatter("b"));

        BOOST_TEST(s2 == "boo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo";
        std::string s2;

        as::find_replace_copy(
            std::back_inserter(s2),
            s1,
            as::first_finder("f"),
            as::const_formatter("b"));

        BOOST_TEST(s2 == "boo");
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range)
    {
        std::string_view const s1 = "foo";
        std::string s2;

        as::find_replace_copy(
            std::back_inserter(s2),
            s1,
            "f",
            "b"
        );

        BOOST_TEST(s2 == "boo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_copy_iterator

BOOST_AUTO_TEST_SUITE(find_replace_copy_sequence)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo";

        auto const s2 = as::find_replace_copy<std::string>(
            s1,
            as::first_finder("f"),
            as::const_formatter("b"));

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "boo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo";

        auto const s2 = as::find_replace_copy<std::string>(
            s1,
            as::first_finder("f"),
            as::const_formatter("b"));

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "boo");
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range)
    {
        std::string_view const s1 = "foo";

        auto const s2 = as::find_replace_copy<std::string>(
            s1,
            "f",
            "b"
        );

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "boo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_copy_sequence

BOOST_AUTO_TEST_SUITE(find_replace_all_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = "foo foo";

        as::find_replace_all(
            s1,
            as::first_finder("foo"),
            as::const_formatter("bar"));

        BOOST_TEST(s1 == "bar bar");
    }

    BOOST_AUTO_TEST_CASE(string_with_range)
    {
        std::string s1 = "foo foo";

        as::find_replace_all(
            s1,
            "foo",
            "bar"
        );

        BOOST_TEST(s1 == "bar bar");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_all_

BOOST_AUTO_TEST_SUITE(find_replace_all_copy_iterator)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo foo";
        std::string s2;

        as::find_replace_all_copy(
            std::back_inserter(s2),
            s1,
            as::first_finder("foo"),
            as::const_formatter("bar"));

        BOOST_TEST(s2 == "bar bar");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo foo";
        std::string s2;

        as::find_replace_all_copy(
            std::back_inserter(s2),
            s1,
            as::first_finder("foo"),
            as::const_formatter("bar"));

        BOOST_TEST(s2 == "bar bar");
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range)
    {
        std::string_view const s1 = "foo foo";
        std::string s2;

        as::find_replace_all_copy(
            std::back_inserter(s2),
            s1,
            "foo",
            "bar"
        );

        BOOST_TEST(s2 == "bar bar");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_all_copy_

BOOST_AUTO_TEST_SUITE(find_replace_all_copy_sequence)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo foo";

        auto const s2 = as::find_replace_all_copy(
            s1,
            as::first_finder("foo"),
            as::const_formatter("bar"));

        BOOST_TEST(s2 == "bar bar");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo foo";

        auto const s2 = as::find_replace_all_copy<std::string>(
            s1,
            as::first_finder("foo"),
            as::const_formatter("bar"));

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "bar bar");
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range)
    {
        std::string_view const s1 = "foo foo";

        auto const s2 = as::find_replace_all_copy<std::string>(
            s1,
            "foo",
            "bar"
        );

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "bar bar");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_all_copy_sequence

BOOST_AUTO_TEST_SUITE_END() // find_replace_

} // namespace testing

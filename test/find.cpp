#include <algorithm/string/find.hpp>

#include "utility.hpp"

#include <algorithm>
#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>
#include <boost/range/as_literal.hpp>

#include <algorithm/string/classification.hpp>
#include <algorithm/string/finder.hpp>

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(find_)

BOOST_AUTO_TEST_SUITE(find_)

    BOOST_AUTO_TEST_CASE(string_with_range)
    {
        std::string const s1 = "foo bar";

        auto const s2 = as::find(s1, "bar");

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[4]);
        BOOST_TEST(s2.end() == &s1[7]);
    }

    BOOST_AUTO_TEST_CASE(string_with_finder)
    {
        std::string const s1 = "foo bar";

        auto const s2 = as::find(s1, as::first_finder("bar"));

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[4]);
        BOOST_TEST(s2.end() == &s1[7]);
    }

    BOOST_AUTO_TEST_CASE(string_with_range_return_string_view)
    {
        std::string const s1 = "foo bar";

        auto const s2 = as::find<std::string_view>(s1, "bar");

        static_assert(type_is<std::string_view>(s2));

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[4]);
        BOOST_TEST(s2.end() == &s1[7]);
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range)
    {
        std::string_view const s1 = "foo bar";

        auto const s2 = as::find(s1, "bar");

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[4]);
        BOOST_TEST(s2.end() == &s1[7]);
    }

    BOOST_AUTO_TEST_CASE(string_view_with_finder)
    {
        std::string_view const s1 = "foo bar";

        auto const s2 = as::find(s1, as::first_finder("bar"));

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[4]);
        BOOST_TEST(s2.end() == &s1[7]);
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range_return_string)
    {
        std::string_view const s1 = "foo bar";

        auto const s2 = as::find<std::string>(s1, "bar");

        static_assert(type_is<std::string>(s2));

        BOOST_CHECK_EQUAL(s2, "bar");
    }

BOOST_AUTO_TEST_SUITE_END() // find_

BOOST_AUTO_TEST_SUITE(find_first_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo bar";

        auto const s2 = as::find_first(s1, "bar");

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[4]);
        BOOST_TEST(s2.end() == &s1[7]);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo bar";

        auto const s2 = as::find_first(s1, "bar");

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[4]);
        BOOST_TEST(s2.end() == &s1[7]);
    }

BOOST_AUTO_TEST_SUITE_END() // find_first_

BOOST_AUTO_TEST_SUITE(ifind_first_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo bar";

        auto const s2 = as::ifind_first(s1, "Bar", std::locale());

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[4]);
        BOOST_TEST(s2.end() == &s1[7]);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo bar";

        auto const s2 = as::ifind_first(s1, "Bar");

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[4]);
        BOOST_TEST(s2.end() == &s1[7]);
    }

BOOST_AUTO_TEST_SUITE_END() // ifind_first_

BOOST_AUTO_TEST_SUITE(find_last_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo barbar";

        auto const s2 = as::find_last(s1, "bar");

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[7]);
        BOOST_TEST(s2.end() == &s1[10]);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo barbar";

        auto const s2 = as::find_last(s1, "bar");

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[7]);
        BOOST_TEST(s2.end() == &s1[10]);
    }

BOOST_AUTO_TEST_SUITE_END() // find_last_

BOOST_AUTO_TEST_SUITE(ifind_last_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo barBar";

        auto const s2 = as::ifind_last(s1, "bar", std::locale());

        BOOST_CHECK_EQUAL(s2, "Bar"_r);
        BOOST_TEST(s2.begin() == &s1[7]);
        BOOST_TEST(s2.end() == &s1[10]);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo barBar";

        auto const s2 = as::ifind_last(s1, "bar");

        BOOST_CHECK_EQUAL(s2, "Bar"_r);
        BOOST_TEST(s2.begin() == &s1[7]);
        BOOST_TEST(s2.end() == &s1[10]);
    }

BOOST_AUTO_TEST_SUITE_END() // ifind_last_

BOOST_AUTO_TEST_SUITE(find_nth_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "barbarbar";

        auto const s2 = as::find_nth(s1, "bar", 1);

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[3]);
        BOOST_TEST(s2.end() == &s1[6]);

        auto const s3 = as::find_nth(s1, "bar", -2);

        BOOST_CHECK_EQUAL(s3, "bar"_r);
        BOOST_TEST(s3.begin() == &s1[3]);
        BOOST_TEST(s3.end() == &s1[6]);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "barbarbar";

        auto const s2 = as::find_nth(s1, "bar", 1);

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[3]);
        BOOST_TEST(s2.end() == &s1[6]);

        auto const s3 = as::find_nth(s1, "bar", -2);

        BOOST_CHECK_EQUAL(s3, "bar"_r);
        BOOST_TEST(s3.begin() == &s1[3]);
        BOOST_TEST(s3.end() == &s1[6]);
    }

BOOST_AUTO_TEST_SUITE_END() // find_nth_

BOOST_AUTO_TEST_SUITE(ifind_nth_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "barBarbar";

        auto const s2 = as::ifind_nth(s1, "bar", 1, std::locale());

        BOOST_CHECK_EQUAL(s2, "Bar"_r);
        BOOST_TEST(s2.begin() == &s1[3]);
        BOOST_TEST(s2.end() == &s1[6]);

        auto const s3 = as::ifind_nth(s1, "bar", -2);

        BOOST_CHECK_EQUAL(s3, "Bar"_r);
        BOOST_TEST(s3.begin() == &s1[3]);
        BOOST_TEST(s3.end() == &s1[6]);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "barBarbar";

        auto const s2 = as::ifind_nth(s1, "bar", 1, std::locale());

        BOOST_CHECK_EQUAL(s2, "Bar"_r);
        BOOST_TEST(s2.begin() == &s1[3]);
        BOOST_TEST(s2.end() == &s1[6]);

        auto const s3 = as::ifind_nth(s1, "bar", -2);

        BOOST_CHECK_EQUAL(s3, "Bar"_r);
        BOOST_TEST(s3.begin() == &s1[3]);
        BOOST_TEST(s3.end() == &s1[6]);
    }

BOOST_AUTO_TEST_SUITE_END() // ifind_nth_

BOOST_AUTO_TEST_SUITE(find_head_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo bar";

        auto const s2 = as::find_head(s1, 3);

        BOOST_CHECK_EQUAL(s2, "foo"_r);
        BOOST_TEST(s2.begin() == &s1[0]);
        BOOST_TEST(s2.end() == &s1[3]);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo bar";

        auto const s2 = as::find_head(s1, 3);

        BOOST_CHECK_EQUAL(s2, "foo"_r);
        BOOST_TEST(s2.begin() == &s1[0]);
        BOOST_TEST(s2.end() == &s1[3]);
    }

BOOST_AUTO_TEST_SUITE_END() // find_head_

BOOST_AUTO_TEST_SUITE(find_tail_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo bar";

        auto const s2 = as::find_tail(s1, 3);

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[4]);
        BOOST_TEST(s2.end() == &s1[7]);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo bar";

        auto const s2 = as::find_tail(s1, 3);

        BOOST_CHECK_EQUAL(s2, "bar"_r);
        BOOST_TEST(s2.begin() == &s1[4]);
        BOOST_TEST(s2.end() == &s1[7]);
    }

BOOST_AUTO_TEST_SUITE_END() // find_tail_

BOOST_AUTO_TEST_SUITE(find_token_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo bar";

        auto const s2 = as::find_token(s1, as::is_space());

        BOOST_CHECK_EQUAL(s2, " "_r);
        BOOST_TEST(s2.begin() == &s1[3]);
        BOOST_TEST(s2.end() == &s1[4]);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo  bar";

        auto const s2 = as::find_token(
                        s1, as::is_space(), as::token_compress_on);

        BOOST_CHECK_EQUAL(s2, "  "_r);
        BOOST_TEST(s2.begin() == &s1[3]);
        BOOST_TEST(s2.end() == &s1[5]);
    }

BOOST_AUTO_TEST_SUITE_END() // find_token_

BOOST_AUTO_TEST_SUITE(find_all_)

    template<typename OutputT>
    void check(OutputT const& o, OutputT const& r)
    {
        check(o, r, [](auto& s) { return s == "foo"; });
    }

    template<typename OutputT, typename Fn>
    void check(OutputT const& o, OutputT const&, Fn pred)
    {
        BOOST_TEST(o.size() == 4);
        BOOST_TEST(std::all_of(o.begin(), o.end(), pred));
    }

    BOOST_AUTO_TEST_CASE(string_to_strings)
    {
        std::string const s1 = "foo foo foo foo";
        std::vector<std::string> o;

        auto& rv = as::find_all(o, s1, as::first_finder("foo"));

        check(o, rv);
    }

    BOOST_AUTO_TEST_CASE(string_to_strings_with_range)
    {
        std::string const s1 = "foo foo foo foo";
        std::vector<std::string> o;

        auto& rv = as::find_all(o, s1, "foo");

        check(o, rv);
    }

    BOOST_AUTO_TEST_CASE(string_to_string_views)
    {
        std::string const s1 = "foo foo foo foo";
        std::vector<std::string_view> o;

        auto& rv = as::find_all(o, s1, as::first_finder("foo"));

        check(o, rv);
    }

    BOOST_AUTO_TEST_CASE(string_to_string_views_with_range)
    {
        std::string const s1 = "foo foo foo foo";
        std::vector<std::string_view> o;

        auto& rv = as::find_all(o, s1, "foo");

        check(o, rv);
    }

    BOOST_AUTO_TEST_CASE(string_to_iterator_ranges_with_range)
    {
        std::string const s1 = "foo foo foo foo";
        std::vector<boost::iterator_range<std::string::const_iterator>> o;

        auto& rv = as::find_all(o, s1, "foo");

        check(o, rv, [](auto& s) { return s == "foo"_r; });
    }

    BOOST_AUTO_TEST_CASE(string_view_to_strings)
    {
        std::string_view const s1 = "foo foo foo foo";
        std::vector<std::string> o;

        auto& rv = as::find_all(o, s1, as::first_finder("foo"));

        check(o, rv);
    }

    BOOST_AUTO_TEST_CASE(string_view_to_strings_with_range)
    {
        std::string_view const s1 = "foo foo foo foo";
        std::vector<std::string> o;

        auto& rv = as::find_all(o, s1, "foo");

        check(o, rv);
    }

    BOOST_AUTO_TEST_CASE(string_view_to_string_views)
    {
        std::string_view const s1 = "foo foo foo foo";
        std::vector<std::string_view> o;

        auto& rv = as::find_all(o, s1, as::first_finder("foo"));

        check(o, rv);
    }

    BOOST_AUTO_TEST_CASE(string_view_to_string_views_with_range)
    {
        std::string_view const s1 = "foo foo foo foo";
        std::vector<std::string_view> o;

        auto& rv = as::find_all(o, s1, "foo");

        check(o, rv);
    }

BOOST_AUTO_TEST_SUITE_END() // find_all_

BOOST_AUTO_TEST_SUITE_END() // find_

} // namespace testing

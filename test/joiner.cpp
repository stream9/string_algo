#include <algorithm/string/joiner.hpp>

#include "utility.hpp"

#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(joiner_)

    BOOST_AUTO_TEST_CASE(elementary)
    {
        as::joiner j { ", " };

        j << "foo" << "bar" << "xyzzy";

        BOOST_TEST(j.str() == "foo, bar, xyzzy");
    }

BOOST_AUTO_TEST_SUITE_END() // joiner_

} // namespace testing

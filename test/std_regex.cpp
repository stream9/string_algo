#include <algorithm/string/std_regex.hpp>

#include "utility.hpp"

#include <regex>

#include <boost/test/unit_test.hpp>

#include <algorithm/string/find.hpp>
#include <algorithm/string/find_erase.hpp>
#include <algorithm/string/find_iterator.hpp>
#include <algorithm/string/find_replace.hpp>

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(std_regex_)

BOOST_AUTO_TEST_CASE(std_regex_finder_)
{
    std::regex re { "a.." };
    auto finder = as::std_regex_finder(re);

    std::string const s1 = "abc";

    auto const result = finder(s1.begin(), s1.end());

    BOOST_CHECK_EQUAL(result.begin(), s1.begin());
    BOOST_CHECK_EQUAL(result.end(), s1.end());

    auto const matches = result.match_results();
    BOOST_TEST(matches.size() == 1);
}

BOOST_AUTO_TEST_CASE(std_regex_formatter_)
{
    std::regex re { "a.." };
    auto finder = as::std_regex_finder(re);

    std::string fmt = "bar";
    auto formatter = as::std_regex_formatter(fmt);

    std::string s1 = "abc";

    auto result = formatter(finder(s1.begin(), s1.end()));

    BOOST_TEST(result == "bar");
}


BOOST_AUTO_TEST_SUITE(find_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo bar";
        std::regex const re { "b.." };
        auto finder = as::std_regex_finder(re);

        auto const v1 = as::find(s1, finder);

        BOOST_CHECK_EQUAL(v1, "bar"_r);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo bar";
        std::regex const re { "b.." };
        auto finder = as::std_regex_finder(re);

        auto const v1 = as::find(s1, finder);

        BOOST_CHECK_EQUAL(v1, "bar"_r);
    }

BOOST_AUTO_TEST_SUITE_END() // find_

BOOST_AUTO_TEST_SUITE(find_iterator_)

    template<typename It>
    void check(It it)
    {
        int i = 0;
        for (auto const& s: it) {
            ++i;
            BOOST_CHECK_EQUAL(s, "foo"_r);
        }

        BOOST_TEST(i == 4);
    }

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        auto it = as::make_find_iterator(s1, finder);

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        auto it = as::make_find_iterator(s1, finder);

        check(it);
    }

BOOST_AUTO_TEST_SUITE_END() // find_iterator_

BOOST_AUTO_TEST_SUITE(find_all_)

    template<typename T>
    void check(T const& o, T const&)
    {
        int i = 0;
        for (auto const& s: o) {
            ++i;
            BOOST_TEST(s == "foo");
        }

        BOOST_TEST(i == 4);
    }

    BOOST_AUTO_TEST_CASE(string_to_strings)
    {
        std::string const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::vector<std::string> o;

        auto& r = as::find_all(o, s1, finder);

        check(o, r);
    }

    BOOST_AUTO_TEST_CASE(string_to_string_views)
    {
        std::string const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::vector<std::string_view> o;

        auto& r = as::find_all(o, s1, finder);

        check(o, r);
    }

    BOOST_AUTO_TEST_CASE(string_view_to_strings)
    {
        std::string_view const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::vector<std::string> o;

        auto& r = as::find_all(o, s1, finder);

        check(o, r);
    }

    BOOST_AUTO_TEST_CASE(string_view_to_string_views)
    {
        std::string_view const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::vector<std::string_view> o;

        auto& r = as::find_all(o, s1, finder);

        check(o, r);
    }

BOOST_AUTO_TEST_SUITE_END() // find_all_

BOOST_AUTO_TEST_SUITE(find_replace_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::string const fmt { "bar" };
        auto formatter = as::std_regex_formatter(fmt);

        as::find_replace(s1, finder, formatter);

        BOOST_TEST(s1 == "bar_foo_foo_foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_

BOOST_AUTO_TEST_SUITE(find_replace_copy_sequence)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::string const fmt { "bar" };
        auto formatter = as::std_regex_formatter(fmt);

        auto const s2 = as::find_replace_copy(s1, finder, formatter);

        BOOST_TEST(s2 == "bar_foo_foo_foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::string const fmt { "bar" };
        auto formatter = as::std_regex_formatter(fmt);

        auto const s2 =
            as::find_replace_copy<std::string>(s1, finder, formatter);

        BOOST_TEST(s2 == "bar_foo_foo_foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_copy_sequence

BOOST_AUTO_TEST_SUITE(find_repalce_copy_iterator)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::string const fmt { "bar" };
        auto formatter = as::std_regex_formatter(fmt);

        std::string s2;
        auto oit = std::back_inserter(s2);
        as::find_replace_copy(oit, s1, finder, formatter);

        BOOST_TEST(s2 == "bar_foo_foo_foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::string const fmt { "bar" };
        auto formatter = as::std_regex_formatter(fmt);

        std::string s2;
        auto oit = std::back_inserter(s2);
        as::find_replace_copy(oit, s1, finder, formatter);

        BOOST_TEST(s2 == "bar_foo_foo_foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_repalce_copy_iterator

BOOST_AUTO_TEST_SUITE(find_replace_all_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::string const fmt { "bar" };
        auto formatter = as::std_regex_formatter(fmt);

        as::find_replace_all(s1, finder, formatter);

        BOOST_TEST(s1 == "bar_bar_bar_bar");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_all_

BOOST_AUTO_TEST_SUITE(find_replace_all_copy_sequence)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::string const fmt { "bar" };
        auto formatter = as::std_regex_formatter(fmt);

        auto const s2 = as::find_replace_all_copy(s1, finder, formatter);

        BOOST_TEST(s2 == "bar_bar_bar_bar");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::string const fmt { "bar" };
        auto formatter = as::std_regex_formatter(fmt);

        auto const s2 =
            as::find_replace_all_copy<std::string>(s1, finder, formatter);

        BOOST_TEST(s2 == "bar_bar_bar_bar");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_all_copy_sequence

BOOST_AUTO_TEST_SUITE(find_replace_all_copy_iterator)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::string const fmt { "bar" };
        auto formatter = as::std_regex_formatter(fmt);

        std::string s2;
        auto oit = std::back_inserter(s2);
        as::find_replace_all_copy(oit, s1, finder, formatter);

        BOOST_TEST(s2 == "bar_bar_bar_bar");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo_foo_foo_foo";
        std::regex const re { "f.." };
        auto finder = as::std_regex_finder(re);

        std::string const fmt { "bar" };
        auto formatter = as::std_regex_formatter(fmt);

        std::string s2;
        auto oit = std::back_inserter(s2);
        as::find_replace_all_copy(oit, s1, finder, formatter);

        BOOST_TEST(s2 == "bar_bar_bar_bar");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_all_copy_iterator


BOOST_AUTO_TEST_SUITE_END() // std_regex_

} // namespace testing

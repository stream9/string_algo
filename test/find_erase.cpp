#include <algorithm/string/find_erase.hpp>

#include "utility.hpp"

#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

#include <algorithm/string/finder.hpp>

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(find_erase_)

BOOST_AUTO_TEST_SUITE(find_erase_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = "foobar";

        as::find_erase(s1, as::first_finder("bar"));

        BOOST_TEST(s1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_with_range)
    {
        std::string s1 = "foobar";

        as::find_erase(s1, "bar");

        BOOST_TEST(s1 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_erase_

BOOST_AUTO_TEST_SUITE(find_erase_copy_iterator)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foobar";
        std::string s2;
        auto inserter = std::back_inserter(s2);

        auto const it =
            as::find_erase_copy(inserter, s1, as::first_finder("bar"));

        static_assert(type_is<decltype(inserter)>(it));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_with_range)
    {
        std::string const s1 = "foobar";
        std::string s2;
        auto inserter = std::back_inserter(s2);

        auto const it = as::find_erase_copy(inserter, s1, "bar");

        static_assert(type_is<decltype(inserter)>(it));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foobar";
        std::string s2;
        auto inserter = std::back_inserter(s2);

        auto const it =
            as::find_erase_copy(inserter, s1, as::first_finder("bar"));

        static_assert(type_is<decltype(inserter)>(it));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range)
    {
        std::string_view const s1 = "foobar";
        std::string s2;
        auto inserter = std::back_inserter(s2);

        auto const it = as::find_erase_copy(inserter, s1, "bar");

        static_assert(type_is<decltype(inserter)>(it));

        BOOST_TEST(s2 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_erase_sequence

BOOST_AUTO_TEST_SUITE(find_erase_copy_sequence)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foobar";

        auto const s2 = as::find_erase_copy(s1, as::first_finder("bar"));

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_with_range)
    {
        std::string const s1 = "foobar";

        auto const s2 = as::find_erase_copy(s1, "bar");

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foobar";

        auto const s2 = as::find_erase_copy<std::string>(
                                    s1, as::first_finder("bar"));

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range)
    {
        std::string_view const s1 = "foobar";

        auto const s2 = as::find_erase_copy<std::string>(s1, "bar");

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_erase_sequence

BOOST_AUTO_TEST_SUITE(find_erase_all_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = "barfoobar";

        as::find_erase_all(s1, as::first_finder("bar"));

        BOOST_TEST(s1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_with_range)
    {
        std::string s1 = "barfoobar";

        as::find_erase_all(s1, "bar");

        BOOST_TEST(s1 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_erase_all

BOOST_AUTO_TEST_SUITE(find_erase_all_copy_iterator)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "barfoobar";
        std::string s2;
        auto oit = std::back_inserter(s2);

        auto it = as::find_erase_all_copy(oit, s1, as::first_finder("bar"));

        static_assert(type_is<decltype(oit)>(it));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_with_range)
    {
        std::string const s1 = "barfoobar";
        std::string s2;
        auto oit = std::back_inserter(s2);

        auto it = as::find_erase_all_copy(oit, s1, "bar");

        static_assert(type_is<decltype(oit)>(it));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "barfoobar";
        std::string s2;
        auto oit = std::back_inserter(s2);

        auto it =
            as::find_erase_all_copy(oit, s1, as::first_finder("bar"));

        static_assert(type_is<decltype(oit)>(it));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range)
    {
        std::string_view const s1 = "barfoobar";
        std::string s2;
        auto oit = std::back_inserter(s2);

        auto it = as::find_erase_all_copy(oit, s1, "bar");

        static_assert(type_is<decltype(oit)>(it));

        BOOST_TEST(s2 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_erase_all_copy_iterator

BOOST_AUTO_TEST_SUITE(find_erase_all_copy_sequence)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "barfoobar";

        auto const s2 = as::find_erase_all_copy(s1, as::first_finder("bar"));

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_with_range)
    {
        std::string const s1 = "barfoobar";

        auto const s2 = as::find_erase_all_copy(s1, "bar");

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "barfoobar";

        auto const s2 =
            as::find_erase_all_copy<std::string>(s1, as::first_finder("bar"));

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range)
    {
        std::string_view const s1 = "barfoobar";

        auto const s2 = as::find_erase_all_copy<std::string>(s1, "bar");

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_erase_all_copy_sequence

BOOST_AUTO_TEST_SUITE_END() // find_erase_

} // namespace testing

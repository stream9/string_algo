#include <algorithm/string/find_iterator.hpp>

#include "utility.hpp"

#include <algorithm/string/finder.hpp>

#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(find_iterator_)

BOOST_AUTO_TEST_SUITE(constructor_)

    template<typename IteratorT>
    void check(IteratorT it)
    {
        auto n = 0;
        for (auto s: it) {
            BOOST_CHECK_EQUAL(s, "foo"_r);
            ++n;
        }
        BOOST_TEST(n == 4);
    }

    BOOST_AUTO_TEST_CASE(range_and_finder)
    {
        std::string const s1 = "foo foo foo foo";

        as::find_iterator<std::string::const_iterator> it {
            s1, as::first_finder("foo")
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_finder_return_string_view)
    {
        std::string const s1 = "foo foo foo foo";

        as::find_iterator<
            std::string::const_iterator, std::string_view> it {
                s1, as::first_finder("foo")
        };

        BOOST_TEST(type_is<std::string_view>(*it));

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_finder_with_deduction_guide)
    {
        std::string const s1 = "foo foo foo foo";

        as::find_iterator it {
            s1, as::first_finder("foo")
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(range_and_range)
    {
        std::string const s1 = "foo foo foo foo";

        as::find_iterator<std::string::const_iterator> it {
            s1, "foo"
        };

        check(it);
    }

    BOOST_AUTO_TEST_CASE(iterator_and_range)
    {
        std::string const s1 = "foo foo foo foo";

        as::find_iterator it {
            s1.begin(), s1.end(), "foo"
        };

        check(it);
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(make_find_iterator_)

    template<typename IteratorT>
    void check(IteratorT it)
    {
        auto n = 0;
        for (auto s: it) {
            BOOST_CHECK_EQUAL(s, "foo"_r);
            ++n;
        }
        BOOST_TEST(n == 4);
    }

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_find_iterator(s1, as::first_finder("foo"));

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_with_range)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_find_iterator(s1, "foo");

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_with_range_return_string_view)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_find_iterator<std::string_view>(s1, "foo");

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = "foo foo foo foo";

        auto it = as::make_find_iterator(s1, as::first_finder("foo"));

        check(it);
    }

    BOOST_AUTO_TEST_CASE(string_view_with_range)
    {
        std::string_view const s1 = "foo foo foo foo";

        auto it = as::make_find_iterator(s1, "foo");

        check(it);
    }

BOOST_AUTO_TEST_SUITE_END() // make_find_iterator_

BOOST_AUTO_TEST_SUITE(ranged_for_support_)

    BOOST_AUTO_TEST_CASE(begin_)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_find_iterator(s1, as::first_finder(" "));

        using std::begin;
        BOOST_TEST((begin(it) == it));
    }

    BOOST_AUTO_TEST_CASE(end_)
    {
        std::string const s1 = "foo foo foo foo";

        auto it = as::make_find_iterator(s1, as::first_finder(" "));

        using std::end;
        BOOST_TEST(end(it).eof());
    }

BOOST_AUTO_TEST_SUITE_END() // ranged_for_support_

BOOST_AUTO_TEST_SUITE_END() // find_iterator_

} // namespace testing

#include <algorithm/string/trim.hpp>

#include "utility.hpp"

#include <string>
#include <string_view>

#include <boost/algorithm/string/trim.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/test/unit_test.hpp>

namespace testing {

namespace as = algorithm::string;

BOOST_AUTO_TEST_SUITE(trim_)

BOOST_AUTO_TEST_SUITE(trim_left_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = " foo ";

        as::trim_left(s1);

        BOOST_TEST(s1 == "foo ");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view s1 = " foo ";

        as::trim_left(s1);

        BOOST_TEST(s1 == "foo ");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_left_

BOOST_AUTO_TEST_SUITE(trim_left_if_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = " foo ";

        as::trim_left_if(s1, as::is_space());

        BOOST_TEST(s1 == "foo ");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view s1 = " foo ";

        as::trim_left_if(s1, as::is_space());

        BOOST_TEST(s1 == "foo ");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_left_if_

BOOST_AUTO_TEST_SUITE(trim_left_copy_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = " foo ";

        auto const s2 = as::trim_left_copy(s1);

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo ");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = " foo ";

        auto const s2 = as::trim_left_copy(s1);

        static_assert(type_is<std::string_view>(s2));

        BOOST_TEST(s2 == "foo ");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_left_copy_

BOOST_AUTO_TEST_SUITE(trim_left_copy_if_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = " foo ";

        auto const s2 =
            as::trim_left_copy_if(s1, as::is_space(std::locale()));

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo ");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = " foo ";

        auto const s2 =
            as::trim_left_copy_if(s1, as::is_space(std::locale()));

        static_assert(type_is<std::string_view>(s2));

        BOOST_TEST(s2 == "foo ");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_left_copy_if_

BOOST_AUTO_TEST_SUITE(trim_right_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = " foo ";

        as::trim_right(s1);

        BOOST_TEST(s1 == " foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view s1 = " foo ";

        as::trim_right(s1);

        BOOST_TEST(s1 == " foo");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_right_

BOOST_AUTO_TEST_SUITE(trim_right_if_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = " foo ";

        as::trim_right_if(s1, as::is_space());

        BOOST_TEST(s1 == " foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view s1 = " foo ";

        as::trim_right_if(s1, as::is_space());

        BOOST_TEST(s1 == " foo");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_right_if_

BOOST_AUTO_TEST_SUITE(trim_right_copy_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = " foo ";

        auto const s2 = as::trim_right_copy(s1);

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == " foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = " foo ";

        auto const s2 = as::trim_right_copy(s1);

        static_assert(type_is<std::string_view>(s2));

        BOOST_TEST(s2 == " foo");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_right_copy_

BOOST_AUTO_TEST_SUITE(trim_right_copy_if_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = " foo ";

        auto const s2 =
            as::trim_right_copy_if(s1, as::is_space(std::locale()));

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == " foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = " foo ";

        auto const s2 =
            as::trim_right_copy_if(s1, as::is_space(std::locale()));

        static_assert(type_is<std::string_view>(s2));

        BOOST_TEST(s2 == " foo");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_right_copy_if_

BOOST_AUTO_TEST_SUITE(trim_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = " foo ";

        as::trim(s1);

        BOOST_TEST(s1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view s1 = " foo ";

        as::trim(s1);

        BOOST_TEST(s1 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_

BOOST_AUTO_TEST_SUITE(trim_if_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = " foo ";

        as::trim_if(s1, as::is_space());

        BOOST_TEST(s1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view s1 = " foo ";

        as::trim_if(s1, as::is_space());

        BOOST_TEST(s1 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_if_

BOOST_AUTO_TEST_SUITE(trim_copy_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = " foo ";

        auto const s2 = as::trim_copy(s1);

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = " foo ";

        auto const s2 = as::trim_copy(s1);

        static_assert(type_is<std::string_view>(s2));

        BOOST_TEST(s2 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_copy_

BOOST_AUTO_TEST_SUITE(trim_copy_if_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string const s1 = " foo ";

        auto const s2 =
            as::trim_copy_if(s1, as::is_space(std::locale()));

        static_assert(type_is<std::string>(s2));

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view const s1 = " foo ";

        auto const s2 =
            as::trim_copy_if(s1, as::is_space(std::locale()));

        static_assert(type_is<std::string_view>(s2));

        BOOST_TEST(s2 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_copy_if_

BOOST_AUTO_TEST_SUITE_END() // trim_

} // namespace algorithm::string::testing
